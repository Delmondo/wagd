extends Reference
class_name Lump


var _buffer: StreamPeerBuffer = null
var name: String = ""


func get_buffer() -> StreamPeerBuffer:
	return _buffer


func _init(_name: String, _data: PoolByteArray = []) -> void:
	name = _name
	_buffer = StreamPeerBuffer.new()
	_buffer.data_array = _data
	pass


func _to_string() -> String:
	return "{ name: %s, buffer: %s }" % [name, _buffer]


func duplicate() -> Lump:
	var dup: PoolByteArray = []
	for b in _buffer.data:
		dup.append(b)

	return get_script().new(name, dup)
