extends SpawnFlags
class_name StrifeSpawnFlags


func apply_flags_to_thing(thing_dict: Dictionary, flag_bits: int) -> void:
	flag_bits &= ~AMBUSH

	if flag_bits & STF_SHADOW:
		thing_dict["renderstyle"] = "shadow"
	if flag_bits & STF_ALT_SHADOW:
		thing_dict["renderstyle"] = "invisible"
	if flag_bits & STF_STANDSTILL:
		thing_dict["standing"] = true
	if flag_bits & STF_AMBUSH:
		thing_dict["ambush"] = true
	if flag_bits & STF_FRIENDLY:
		thing_dict["friend"] = true

	._set_things_class_availability(thing_dict, 1, 3)
	pass