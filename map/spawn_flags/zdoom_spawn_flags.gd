extends SpawnFlags
class_name ZdoomSpawnFlags


func apply_flags_to_thing(thing_dict: Dictionary, flag_bits: int) -> void:
	if flag_bits & SHADOW:
		thing_dict["renderstyle"] = "shadow"
	if flag_bits & ALT_SHADOW:
		thing_dict["renderstyle"] = "invisible"
	if flag_bits & STANDSTILL:
		thing_dict["standing"] = true
	if flag_bits & AMBUSH:
		thing_dict["ambush"] = true
	if flag_bits & FRIENDLY:
		thing_dict["friend"] = true

	pass
