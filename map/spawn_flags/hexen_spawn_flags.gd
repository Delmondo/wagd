extends SpawnFlags
class_name HexenSpawnFlags


func apply_flags_to_thing(thing_dict: Dictionary, flag_bits: int) -> void:
	.apply_flags_to_thing(thing_dict, flag_bits)

	if !(flag_bits & CLASS1):
		thing_dict["class1"] = false
	if !(flag_bits & CLASS2):
		thing_dict["class2"] = false
	if !(flag_bits & CLASS3):
		thing_dict["class3"] = false

	pass
