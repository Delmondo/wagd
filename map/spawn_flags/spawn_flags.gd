extends Reference
class_name SpawnFlags


enum {
	EASY					= 0x0001,
	MEDIUM					= 0x0002,
	HARD					= 0x0004,

	AMBUSH					= 0x0008,
	DORMANT					= 0x0010,

	CLASS1					= 0x0020,
	CLASS2					= 0x0040,
	CLASS3					= 0x0080,

	SINGLE					= 0x0100,
	COOPERATIVE				= 0x0200,
	DEATHMATCH				= 0x0400,

	SHADOW					= 0x0800,
	ALT_SHADOW				= 0x1000,
	FRIENDLY				= 0x2000,
	STANDSTILL				= 0x4000,
	STRIFE_SOMETHING		= 0x8000,

	SECRET					= 0x080000,
	NO_INFIGHTING			= 0x100000,

	BTF_NOT_SINGLE			= 0x0010,
	BTF_NOT_DEATHMATCH		= 0x0020,
	BTF_NOT_COOPERATIVE		= 0x0040,
	BTF_FRIENDLY			= 0x0080,
	BTF_BAD_EDITOR_CHECK	= 0x0100,

	# strife thing flags

	STF_STANDSTILL			= 0x0008,
	STF_AMBUSH				= 0x0020,
	STF_FRIENDLY			= 0x0040,
	STF_SHADOW				= 0x0100,
	STF_ALT_SHADOW			= 0x0200
}


func _apply_skills_to_thing(thing_dict: Dictionary, flag_bits: int) -> void:
	if flag_bits & EASY:
		thing_dict["skill1"] = true
		thing_dict["skill2"] = true

	if flag_bits & MEDIUM:
		thing_dict["skill3"] = true

	if flag_bits & HARD:
		thing_dict["skill4"] = true
		thing_dict["skill5"] = true
	pass


func _apply_gamemodes_to_thing(thing_dict: Dictionary, flag_bits: int) -> void:
	if flag_bits & BTF_NOT_DEATHMATCH:
		thing_dict["dm"] = false

	if flag_bits & BTF_NOT_COOPERATIVE:
		thing_dict["coop"] = false

	if flag_bits & BTF_NOT_SINGLE:
		thing_dict["single"] = false

	pass


func _apply_friendliness_to_thing(thing_dict: Dictionary, flag_bits: int) -> void:
	if flag_bits & BTF_FRIENDLY:
		thing_dict["friend"] = true

	pass


func _apply_class_availability_to_thing(thing_dict: Dictionary, start: int, end: int, available: bool = true) -> void:
	for i in range(start, end + 1):
		if thing_dict.has("class" + str(i)):
			thing_dict["class" + str(i)] = available
	pass


func _bad_editor_check(flag_bits: int) -> int:
	if flag_bits & BTF_BAD_EDITOR_CHECK:
		flag_bits &= 0x1f

	return flag_bits


func apply_flags_to_thing(thing_dict: Dictionary, flag_bits: int) -> void:
	flag_bits = _bad_editor_check(flag_bits)

	_apply_skills_to_thing(thing_dict, flag_bits)
	_apply_gamemodes_to_thing(thing_dict, flag_bits)
	_apply_friendliness_to_thing(thing_dict, flag_bits)
	_apply_class_availability_to_thing(thing_dict, 1, 3)
	pass
