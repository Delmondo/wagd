extends Reference
class_name MapFactory


static func get_map(raw_map_data: Dictionary) -> DoomMap:
	if raw_map_data.has("format"):
		match raw_map_data["format"]:
			Wad.MapFormat.DOOM:
				return DoomMap.new(raw_map_data)
			Wad.MapFormat.HEXEN:
				return HexenMap.new(raw_map_data)

	return null
