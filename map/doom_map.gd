extends Reference
class_name DoomMap


var spawn_flags:	SpawnFlags	= SpawnFlags.new()
var line_flags:		LineFlags	= LineFlags.new()

var things:		Array	= []
var linedefs:	Array	= []
var sidedefs:	Array	= []
var vertices:	Array	= []
var segs:		Array	= []
var subsectors:	Array	= []
var nodes:		Array	= []
var sectors:	Array	= []
var reject:		Array	= []
var blockmap:	Array	= []
var behavior:	Array	= []


func _init(raw_map_data: Dictionary = {}) -> void:
	if raw_map_data:
		for k in raw_map_data:
			if raw_map_data[k] is StreamPeerBuffer:
				match k:
					"THINGS":
						things = _read_things(raw_map_data[k])
					"LINEDEFS":
						linedefs = _read_linedefs(raw_map_data[k])
					"SIDEDEFS":
						sidedefs = _read_sidedefs(raw_map_data[k])
					"VERTEXES":
						vertices = _read_vertices(raw_map_data[k])
					"SEGS":
						segs = _read_segs(raw_map_data[k])
					"SSECTORS":
						subsectors = _read_subsectors(raw_map_data[k])
					"NODES":
						nodes = _read_nodes(raw_map_data[k])
	pass


func get_namespace() -> String:
	return "Doom"


func _read_things(buffer: StreamPeerBuffer) -> Array:
	var new_things: Array = []

	while buffer.get_available_bytes():
		var thing: Dictionary = get_defaut_thing()

		thing["x"] = buffer.get_16()
		thing["y"] = buffer.get_16()
		thing["angle"] = buffer.get_u16()
		thing["type"] = buffer.get_u16()
		spawn_flags.apply_flags_to_thing(thing, buffer.get_u16())

		new_things.append(thing)

	return new_things


func _read_linedefs(buffer: StreamPeerBuffer) -> Array:
	var new_linedefs: Array = []

	while buffer.get_available_bytes():
		var linedef: Dictionary = get_defaut_linedef()

		linedef["v1"] = buffer.get_u16()
		linedef["v2"] = buffer.get_u16()
		line_flags.apply_flags_to_line(linedef, buffer.get_u16())
		linedef["special"] = buffer.get_u16()
		linedef["id"] = buffer.get_u16()
		linedef["sidefront"] = buffer.get_u16()
		linedef["sideback"] = buffer.get_u16()

		new_linedefs.append(linedef)

	return new_linedefs


func _read_sidedefs(buffer: StreamPeerBuffer) -> Array:
	var new_sidedefs: Array = []

	while buffer.get_available_bytes():
		var sidedef: Dictionary = get_defaut_sidedef()

		sidedef["offsetx"] = buffer.get_16()
		sidedef["offsety"] = buffer.get_16()
		sidedef["texturetop"] = buffer.get_string(8)
		sidedef["texturebottom"] = buffer.get_string(8)
		sidedef["texturemiddle"] = buffer.get_string(8)
		sidedef["sector"] = buffer.get_u16()

		new_sidedefs.append(sidedef)

	return new_sidedefs


func _read_vertices(buffer: StreamPeerBuffer) -> Array:
	var new_vertices: Array = []

	while buffer.get_available_bytes():
		var vertex: Dictionary = get_defaut_vertex()

		vertex["x"] = buffer.get_16()
		vertex["y"] = buffer.get_16()

		new_vertices.append(vertex)

	return new_vertices


func _read_segs(buffer: StreamPeerBuffer) -> Array:
	var new_segs: Array = []

	while buffer.get_available_bytes():
		var seg: Dictionary = {}

		seg["v1"] = buffer.get_u16()
		seg["v2"] = buffer.get_u16()
		seg["angle"] = buffer.get_16()
		seg["line"] = buffer.get_u16()
		seg["side"] = buffer.get_16()
		seg["offset"] = buffer.get_16()

	return new_segs


func _read_subsectors(buffer: StreamPeerBuffer) -> Array:
	var new_subsectors: Array = []

	while buffer.get_available_bytes():
		var subsector: Dictionary = {}

		subsector["numsegs"] = buffer.get_16()
		subsector["firstseg"] = buffer.get_16()

	return new_subsectors


func _read_nodes(buffer: StreamPeerBuffer) -> Array:
	var new_nodes: Array = []

	while buffer.get_available_bytes():
		var node: Dictionary = {}

		node["partx"] = buffer.get_16()
		node["party"] = buffer.get_16()
		node["dx"] = buffer.get_16()
		node["dy"] = buffer.get_16()
		node["bboxright"] = buffer.get_64()
		node["bboxleft"] = buffer.get_64()
		node["childright"] = buffer.get_16()
		node["childleft"] = buffer.get_16()

	return new_nodes


func _read_sectors(buffer: StreamPeerBuffer) -> Array:
	var new_sectors: Array = []

	while buffer.get_available_bytes():
		var sector: Dictionary = get_defaut_sector()

		sector["heightfloor"] = buffer.get_16()
		sector["heightceiling"] = buffer.get_16()
		sector["texturefloor"] = buffer.get_string(8)
		sector["textureceiling"] = buffer.get_string(8)
		sector["lightlevel"] = buffer.get_16()
		sector["special"] = buffer.get_u16()
		sector["id"] = buffer.get_u16()

	return new_sectors


func get_defaut_thing() -> Dictionary: return {
	"id":			0,
	"x":			null,
	"y":			null,
	"height":		0.0,
	"angle":		0,
	"skill1":		false,
	"skill2":		false,
	"skill3":		false,
	"skill4":		false,
	"skill5":		false,
	# start zdoom extensions
	"skill6":		false,
	"skill7":		false,
	"skill8":		false,
	"skill9":		false,
	"skill10":		false,
	"skill11":		false,
	"skill12":		false,
	"skill13":		false,
	"skill14":		false,
	"skill15":		false,
	"skill16":		false,
	# end zdoom extensions
	"ambush":		false,
	"single":		false,
	"dm":			false,
	"coop":			false,

	# [GZ] MBF friend flag not supported in Strife/Heretic/Hexen namespaces.
	"friend":		false,

	# [GZ] Hexen flags; not supported in Doom/Strife/Heretic namespaces.
	"dormant":		false,
	"class1":		false,
	"class2":		false,
	"class3":		false,
	# start zdoom extensions
	"class4":		false,
	"class5":		false,
	"class6":		false,
	"class7":		false,
	"class8":		false,
	"class9":		false,
	"class10":		false,
	"class11":		false,
	"class12":		false,
	"class13":		false,
	"class14":		false,
	"class15":		false,
	"class16":		false,
	# end zdoom extensions

	# [GZ] Strife specific flags. Support for other games is not defined
	# by default and these flags should be ignored when reading maps
	# not for the Strife namespace or maps for a port which supports
	# these flags

	"standing":		false,
	"strifeally":	false,
	"translucent":	false,
	"invisible":	false,

	# [GZ] Note: suggested editor defaults for all skill, gamemode, and
	# player class flags is true rather than the UDMF default of false.

	# [GZ] Thing special semantics are only defined for the Hexen
	# namespace or ports which implement this feature in their own
	# namespace.

	"special":		0,
	"arg0":			0,
	"arg1":			0,
	"arg2":			0,
	"arg3":			0,
	"arg4":			0,

	"comment":		"",

	# zdoom-specific properties

	"conversation":		0,
	"countsecrect":		false,
	"arg0str":			"",
	"gravity":			1.0,
	"health":			1.0,
	"renderstyle":		"normal",
	"fillcolor":		0x000000,
	"alpha":			1.0,
	"score":			0,
	"pitch":			0,
	"roll":				0,
	"scalex":			0.0, # 0 = ignored
	"scaley":			0.0,
	"scale":			0.0,
	"floatbobphase":	-1
}


func get_defaut_vertex() -> Dictionary: return {
	"x":		null,
	"y":		null,
	# zdoom-specific properties
	"zfloor":	null,
	"zceiling":	null
}


func get_defaut_linedef() -> Dictionary: return {
	"id":				-1,
	"v1":				null,
	"v2":				null,

	"blocking":			false,
	"blockmonsters":	false,
	"twosided":			false,
	"dontpegtop":		false,
	"dontpegbottom":	false,
	"secret":			false,
	"blocksound":		false,
	"dontdraw":			false,
	"mapped":			false,

	# [GZ] BOOM passuse flag not supported in Strife/Heretic/Hexen namespaces.

	"passuse":			false,

	# [GZ] Strife specific flags. Support for other games is not defined by
	# default and these flags should be ignored when reading maps not for the
	# Strife namespace or maps for a port which supports these flags.

	"translucent":		false,
	"jumpover":			false,
	"blockfloaters":	false,

	# [GZ] Note: SPAC flags should be set false in Doom/Heretic/Strife namespace
	# maps. Specials in those games do not support this mechanism and instead
	# imply activation parameters through the special number. All flags default
	# to false.

	"playercross":		false,
	"playeruse":		false,
	"monstercross":		false,
	"monsteruse":		false,
	"impact":			false,
	"playerpush":		false,
	"monsterpush":		false,
	"missilecross":		false,
	"repeatspecial":	false,

	"special":			0,
	"arg0":				0,
	"arg1":				1,
	"arg2":				0,
	"arg3":				0,
	"arg4":				0,

	"sidefront":		null,
	"sideback":			-1,

	"comment":			"",

	# zdoom-specific properties

	"alpha":				1.0,
	"renderstyle":			"translucent",
	"playeruseback":		false,
	"anycross":				false,
	"monsteractivate":		false,
	"blockplayers":			false,
	"blockeverything":		false,
	"firstsideonly":		false,
	"zoneboundary":			false,
	"clipmidtex":			false,
	"wrapmidtex":			false,
	"midtex3d":				false,
	"midtex3dimpassible":	false,
	"checkswitchrange":		false,
	"blockprojectiles":		false,
	"blockuse":				false,
	"blocksight":			false,
	"blockhitscan":			false,
	"locknumber":			0,
	"arg0str":				"",
	"moreids":				"", # [GZ] additional line IDs, specified as a space
								# separated list of numbers
								# (e.g. "2 666 1003 4505")
	"transparent":			false,	# [GZ] true = line is a Strife transparent
									# line (alpha 0.25)
	"automapstyle":			0, # refer to https://github.com/coelckers/gzdoom/blob/master/specs/udmf_zdoom.txt
	"revealed":				false,
	"noskywalls":			false,
	"drawfullheight":		false,
	"health":				0,
	"healthgroup":			0,
	"damagespecial":		false,	# [GZ] this line will call special if having
									# health>0 and recieving damage
	"deathspecial":			false

	# [GZ] * Note about arg0str

	# For lines with ACS specials (80-86 and 226), if arg0str is present and non-null, it
	# will be used as the name of the script to execute, and arg0 will be ignored.
}


func get_defaut_sidedef() -> Dictionary: return {
	"offsetx":			0,
	"offsety":			0,

	"texturetop":		"-",
	"texturebottom":	"-",
	"texturemiddle":	"-",

	"sector":			null,

	"comment":			"",

	# zdoom-specific properties

	"scalex_top":				1.0,
	"scaley_top":				1.0,
	"scalex_mid":				1.0,
	"scaley_mid":				1.0,
	"scalex_bottom":			1.0,
	"scaley_bottom":			1.0,
	"offsetx_top":				0.0,
	"offsety_top":				0.0,
	"offsetx_mid":				0.0,
	"offsety_mid":				0.0,
	"offsetx_bottom":			0.0,
	"offsety_bottom":			0.0,

	"light":					0,
	"lightabsolute":			false,	# [GZ] true = 'light' is an absolute value.
										# Default is relative to the owning sector's
										# light level.
	"lightfog":					false,
	"nofakeconstrast":			false,
	"smoothlighting":			false,
	"clipmidtex":				false,
	"wrapmidtx":				false,
	"nodecals":					false,

	"nogradient_top":			false,
	"flipgradient_top":			false,
	"clampgradient_top":		false,
	"useowncolors_top":			false,
	"uppercolor_top":			0x00000000,
	"lowercolor_top":			0x00000000,

	"nogradient_mid":			false,
	"flipgradient_mid":			false,
	"clampgradient_mid":		false,
	"useowncolors_mid":			false,
	"uppercolor_mid":			0x00000000,
	"lowercolor_mid":			0x00000000,

	"nogradient_bottom":		false,
	"flipgradient_bottom":		false,
	"clampgradient_bottom":		false,
	"useowncolors_bottom":		false,
	"uppercolor_bottom":		0x00000000,
	"lowercolor_bottom":		0x00000000,

	"colorscalefactor_top":		1.0,
	"colorscalefactor_mid": 	1.0,
	"colorscalefactor_bottom": 	1.0,

	"useowncoloradd_top":		false,
	"useowncoloradd_mid":		false,
	"useowncoloradd_bottom":	false,
	"coloradd_top":				0x00000000,
	"coloradd_mid":				0x00000000,
	"coloradd_bottom":			0x00000000,
	"colorization_top":			null,
	"colorization_mid":			null,
	"colorization_bottom":		null
}


func get_defaut_sector() -> Dictionary: return {
	"heightfloor":				0,
	"heightceiling":			0,

	"texturefloor":				null,
	"textureceiling":			null,

	"lightlevel":				160,

	"special":					0,
	"id":						0,

	"comment":					"",

	# zdoom-specific properties

	"xpanningfloor":			0.0,
	"ypanningfloor":			0.0,
	"xpanningceiling":			0.0,
	"ypanningceiling":			0.0,
	"xscalefloor":				1.0,
	"yscalefloor":				1.0,
	"xscaleceiling":			1.0,
	"yscaleceiling":			1.0,
	"rotationfloor":			0.0,
	"rotationceiling":			0.0,
	"ceilingplane_a":			null,	# [GZ] Define the plane equation for the
										# sector's ceiling. Default is a
										# horizontal plane at 'heightceiling'.
	"ceilingplane_b":			null,	# [GZ] 'heightceiling' will still be
										# used to calculate texture alignment.
	"ceilingplane_c":			null,	# [GZ] The plane equation will only be
										# used if all 4 values are given.
	"ceilingplane_d":			null,	# [GZ] The plane is defined as
										# a*x + b*y + c*z + d = 0
										# with the normal vector pointing
										# downward.
	"floorplane_a":				null,
	"floorplane_b":				null,
	"floorplane_c":				null,
	"floorplane_d":				null,
	"lightfloor":				0,
	"lightceiling":				0,
	"lightfloorabsolute":		false,
	"lightceilingabsolute":		false,
	"alphafloor":				1.0,
	"alphaceiling":				1.0,
	"renderstylefloor":			"translucent",
	"renderstyleceiling":		"translucent",
	"gravity":					1.0,
	"lightcolor":				0xffffffff,
	"fadecolor":				0x00000000,
	"desaturation":				0.0,
	"silent":					false,
	"nofallingdamage":			false,
	"noattack":					false,
	"dropactors":				false,
	"norespawn":				false,
	"soundsequence":			"",
	"hidden":					false,
	"waterzone":				false,
	"moreids":					"",	# see linedefs moreids
	"damageamount":				0,

	"damagetype":				"none",
	"damageinterval":			32,
	"leakiness":				0,	# [GZ] Probability of leaking through
									# radiation suit (0 = never, 256 = always),
									# default = 0.
	"damageterraineeffect":		false,
	"damagehazard":				false,
	"floorterrain":				"",	# Sets the terrain for the sector's floor.
									# Default = 'use the flat texture's terrain
									# definition.'
	"ceilingterrain":			"",
	"floor_reflect":			0.0,
	"ceiling_reflect":			0.0,
	"fogdensity":				0,
	"floorglowcolor":			-1,
	"floorglowheight":			0.0,
	"ceilingglowcolor":			-1,
	"ceilingglowheight":		0.0,
	"color_floor":				0xffffffff,
	"color_ceiling":			0xffffffff,
	"color_walltop":			0xffffffff,
	"color_wallbottom":			0xffffffff,
	"color_sprites":			0xffffffff,

	"coloradd_floor":			0x00000000,
	"coloradd_ceiling":			0x00000000,
	"coloradd_sprites":			0x00000000,
	"coloradd_walls":			0x00000000,

	"colorization_floor":		null,
	"colorization_ceiling":		null,

	"noskywalls":				false,

	"portal_ceil_blocksound":	false,
	"portal_ceil_disabled":		false,
	"portal_ceil_nopass":		false,
	"portal_ceil_norender":		false,
	"portal_ceil_overlaytype":	"translucent",
	"portal_floor_blocksound":	false,
	"portal_floor_disabled":	false,
	"portal_floor_nopass":		false,
	"portal_floor_norender":	false,
	"portal_floor_overlaytype":	"translucent",

	"healthfloor":				0,
	"healthfloorgroup":			0,
	"healthceiling":			0,
	"healthceilinggroup":		0
}
