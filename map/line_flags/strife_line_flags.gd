extends LineFlags
class_name StrifeLineFlags


func apply_flags_to_line(line: Dictionary, flag_bits: int) -> void:
	.apply_doom_flags_to_line(line, flag_bits)

	if flag_bits & RAILING:
		line["jumpover"] = true

	if flag_bits & BLOCK_FLOATERS:
		line["blockfloating"] = true

	if flag_bits & MapLineFlags.TRANSLUCENT_STRIFE:
		line["translucent"] = true

	if flag_bits & MapLineFlags.TRANSPARENT_STRIFE:
		line["transparent"] = true

	if flag_bits & CLIP_MIDTEX:
		line["clipmidtex"] = true

	pass
