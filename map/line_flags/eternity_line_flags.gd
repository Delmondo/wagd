extends BoomLineFlags
class_name EternityLineFlags


func apply_flags_to_line(line: Dictionary, flag_bits: int) -> void:
	._apply_flags_to_line(line, flag_bits)

	if flag_bits & BLOCK_FLOATERS:
		line["midtex3d"] = true

	pass
