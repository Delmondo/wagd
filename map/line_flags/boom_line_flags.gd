extends LineFlags
class_name BoomLineFlags


func apply_flags_to_line(line: Dictionary, flag_bits: int) -> void:
	._apply_doom_flags_to_line(line, flag_bits)

	if flag_bits & RAILING:
		line["passuse"] = true

	pass
