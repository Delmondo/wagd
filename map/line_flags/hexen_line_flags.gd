extends LineFlags
class_name HexenLineFlags


func apply_flags_to_line(line: Dictionary, flag_bits: int) -> void:
	._apply_doom_flags_to_line(line, flag_bits)

	if flag_bits & REPEAT_SPECIAL:
		line["repeatspecial"] = true

	if flag_bits & Spac.USE:
		line["playeruse"] = true

	if flag_bits & Spac.MONSTER_CROSS:
		line["monstercross"] = true

	if flag_bits & Spac.IMPACT:
		line["impact"] = true

	if flag_bits & Spac.PUSH:
		line["playerpush"] = true

	if flag_bits & Spac.PROJECTILE_CROSS:
		line["missilecross"]  = true

	if flag_bits & Spac.USE_THROUGH:
		line["blocking"] = true

	if flag_bits & MONSTERS_CAN_ACTIVATE:
		line["monsteractivate"] = true

	if flag_bits & BLOCK_PLAYERS:
		line["blockplayers"] = true

	if flag_bits & BLOCK_EVERYTHING:
		line["blockeverything"] = true

	if flag_bits & Spac.CROSS:
		line["playercross"] = true

	if flag_bits & Spac.ANY_CROSS:
		line["anycross"] = true

	if flag_bits & Spac.MONSTER_USE:
		line["monsteruse"] = true

	if flag_bits & Spac.MONSTER_PUSH:
		line["monsterpush"] = true

	if flag_bits & FIRST_SIDE_ONLY:
		line["firstsideonly"] = true

	if flag_bits & ZONE_BOUNDARY:
		line["zoneboundary"] = true

	if flag_bits & WRAP_MIDTEX:
		line["wrapmidtx"] = true

	if flag_bits & CHECK_SWITCH_RANGE:
		line["checkswitchrange"] = true

	if flag_bits & BLOCK_PROJECTILE:
		line["blockprojectiles"] = true

	if flag_bits & BLOCK_USE:
		line["blockuse"] = true

	if flag_bits & BLOCK_SIGHT:
		line["blocksight"] = true

	if flag_bits & BLOCK_HITSCAN:
		line["blockhitscan"] = true

	if flag_bits & THREED_MIDTEX_IMPASS:
		line["midtex3dimpassible"] = true

	pass
