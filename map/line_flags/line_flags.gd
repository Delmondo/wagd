extends Reference
class_name LineFlags


enum {
	BLOCKING				= 0x00000001,
	BLOCK_MONSTERS			= 0x00000002,
	TWO_SIDED				= 0x00000004,
	DONT_PEG_TOP			= 0x00000008,
	DONT_PEG_BOTTOM			= 0x00000010,
	SECRET					= 0x00000020,
	SOUND_BLOCK				= 0x00000040,
	DONT_DRAW				= 0x00000080,
	MAPPED					= 0x00000100,
	REPEAT_SPECIAL			= 0x00000200,
	# this...
	ADD_TRANS				= 0x00000400,

	# these are only used internally apparently
	COMPAT_SIDE				= 0x00000800,
	NO_SKY_WALLS			= 0x00001000,

	MONSTERS_CAN_ACTIVATE	= 0x00002000,
	BLOCK_PLAYERS			= 0x00004000,
	BLOCK_EVERYTHING		= 0x00008000,
	ZONE_BOUNDARY			= 0x00010000,
	RAILING					= 0x00020000,
	BLOCK_FLOATERS			= 0x00040000,
	CLIP_MIDTEX				= 0x00080000,
	WRAP_MIDTEX				= 0x00100000,
	THREED_MIDTEX			= 0x00200000,
	CHECK_SWITCH_RANGE		= 0x00400000,
	FIRST_SIDE_ONLY			= 0x00800000,
	BLOCK_PROJECTILE		= 0x01000000,
	BLOCK_USE				= 0x02000000,
	BLOCK_SIGHT				= 0x04000000,
	BLOCK_HITSCAN			= 0x08000000,
	THREED_MIDTEX_IMPASS	= 0x10000000,
	REVEALED				= 0x20000000,
	DRAW_FULL_HEIGHT		= 0x40000000,
	PORTAL_CONNECT			= 0x80000000
}

enum Spac {
	CROSS				= 1,	# [GZ] when player crosses line
	USE					= 2,	# [GZ] when player uses line
	MONSTER_CROSS		= 4,	# [GZ] when monster crosses line
	IMPACT				= 8,	# [GZ] when projectile hits line
	PUSH				= 16,	# [GZ] when palyer pushes like
	PROJECTILE_CROSS	= 32,	# [GZ] when projectile crosses line
	USE_THROUGH			= 64,	# [GZ] when player uses line (doesn't block)
	ANY_CROSS			= 128,	# [GZ] when anything without the MF2_TELEPORT flag crosses the line
	MONSTER_USE			= 512,	# [GZ] monsters can use
	MONSTER_PUSH		= 1024,	# [GZ] monsters can push
	USE_BACK			= 2048,	# [GZ] can be used from the back side
	DAMAGE				= 4096,	# [ZZ] when linedef recieves damage
	DEATH				= 8192,	# [ZZ] when linedef recieves damage and has 0 health

	PLAYER_ACTIVATE 	= (1 | 2 | 8 | 16 | 64 | 128 | 2048)
}

# man wtf is this

enum MapLineFlags {
	SPAC_SHIFT				= 10,
	SPAC_MASK				= 0x1c00, # [GZ] Hexen's activator mask.

	# [RH] BOOM's ML_PASSUSE flag (conflicts with ML_REPEATSPECIAL)
	PASSUSE_BOOM			= 0x0200,
	THREED_MIDTEX_ETERNITY	= 0x0400,

	# [GZ] if this bit is set, then all non-original-Doom bits are cleared when
	# translating the line. Only applies when playing Doom with Doom-format
	# maps. Hexen format maps and the other games are not affected by this.
	RESERVED_ETERNITY		= 0x0800,

	# [RH] Extra flags for Strife
	RAILING_STRIFE			= 0x0200,
	BLOCK_FLOATERS_STRIFE	= 0x0400,
	TRANSPARENT_STRIFE		= 0x0800,
	TRANSLUCENT_STRIFE		= 0x1000
}


static func get_spac(flags: int) -> int:
	return (flags & MapLineFlags.SPAC_MASK) >> MapLineFlags.SPAC_SHIFT


func _apply_doom_flags_to_line(line: Dictionary, flag_bits: int) -> void:
	if flag_bits & BLOCKING:
		line["blocking"] = true

	if flag_bits & BLOCK_MONSTERS:
		line["blockmonsters"] = true

	if flag_bits & TWO_SIDED:
		line["twosided"] = true

	if flag_bits & DONT_PEG_TOP:
		line["dontpegtop"] = true

	if flag_bits & DONT_PEG_BOTTOM:
		line["dontpegbottom"] = true

	if flag_bits & SECRET:
		line["secret"] = true

	if flag_bits & SOUND_BLOCK:
		line["blocksound"] = true

	if flag_bits & DONT_DRAW:
		line["dontdraw"] = true

	if flag_bits & MAPPED:
		line["mapped"] = true

	pass


func apply_flags_to_line(line: Dictionary, flag_bits: int) -> void:
	_apply_doom_flags_to_line(line, flag_bits)
	pass
