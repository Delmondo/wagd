extends Reference
class_name Wad


enum Type { PWAD, IWAD }
enum MapFormat { DOOM, HEXEN }

const _MAP_LUMP_NAMES: Array = [
	"THINGS",
	"LINEDEFS",
	"SIDEDEFS",
	"VERTEXES",
	"SEGS",
	"SSECTORS",
	"NODES",
	"SECTORS",
	"REJECT",
	"BLOCKMAP",
	"BEHAVIOR"
]

var _lump_info: Array = []
var _logical_lump_info: Dictionary = {}

var _files: Dictionary = {}
var _cache_size: int = 0
# wad info
var _num_entries: int = 0
var _dir_offset: int = 0

var type: int = Type.PWAD


func get_num_entries() -> int:
	return _num_entries


func _init(paths: PoolStringArray) -> void:
	for path in paths:
		var file: File = File.new()
		_files[path] = file

		var error: int = file.open(path, File.READ)
		if error:
			# warning-ignore:return_value_discarded
			_files.erase(path)

			push_error("WadReader._init(): Could not open "
				+ path + ". Error code: " + str(error) + ".")

			continue

		var wad_str_buf: PoolByteArray = []

		for _i in range(4):
			wad_str_buf.append(file.get_8())

		if wad_str_buf.get_string_from_ascii() == "IWAD":
			type = Type.IWAD

		_num_entries = file.get_32()
		_dir_offset = file.get_32()

		file.seek(_dir_offset)

		# read lump headers

		for _i in range(_num_entries):
			var file_pos: int = file.get_32()
			var size: int = file.get_32()

			var name_buf: PoolByteArray = []
			for _j in range(8):
				name_buf.append(file.get_8())

			var name: String = name_buf.get_string_from_ascii()

			var li: LumpInfo = LumpInfo.new(
				name,
				file,
				file_pos,
				_i,
				size
			)

			_lump_info.append(li)

			if !_logical_lump_info.has(name):
				_logical_lump_info[name] = []

			_logical_lump_info[name].push_front(li)

	pass


func free() -> void:
	for f in _files.values():
		if f.is_open():
			f.close()

	.free()
	pass


func read_lump(index: int) -> Lump:
	if index < _lump_info.size() && _lump_info[index]:
		return _lump_info[index].read_lump()

	return null


func read_lump_for_name(name: String) -> Lump:
	if _logical_lump_info.has(name):
		return _logical_lump_info[name][0].read_lump()

	return null


func get_lump_info(index: int) -> LumpInfo:
	if index < _lump_info.size() && _lump_info[index]:
		return _lump_info[index].duplicate()

	return null


func get_lump_info_from_name(name: String) -> LumpInfo:
	if _logical_lump_info.has(name):
		return _logical_lump_info[name][0].duplicate()

	return null


func read_raw_map_data(index: int) -> Dictionary:
	var start_idx: int = index + 1

	if start_idx >= _num_entries:
		return {}

	var map: Dictionary = { "format": MapFormat.DOOM }

	for i in range(start_idx, _num_entries):
		var cur_lump_info: LumpInfo = _lump_info[i]

		# expects map lumps to appear in the order they appear in
		# _MAP_LUMP_NAMES
		if cur_lump_info.name != _MAP_LUMP_NAMES[i - start_idx]:
			break

		if cur_lump_info.name == "BEHAVIOR":
			map["format"] = MapFormat.HEXEN

		map[cur_lump_info.name] = read_lump(i).get_buffer()

	return map


func read_raw_map_data_for_name(map_lump_name: String) -> Dictionary:
	if _logical_lump_info.has(map_lump_name):
		return read_raw_map_data(_logical_lump_info[map_lump_name][0].index)

	return {}
