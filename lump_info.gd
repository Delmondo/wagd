extends Reference
class_name LumpInfo


var _file_ptr: File = null
var _file_pos: int = 0

var name: String = ""
var index: int = 0
var size: int = 0


func _init(	_name: String,
			__file_ptr: File,
			__file_pos: int,
			_index: int,
			_size: int) -> void:

	name = _name
	_file_ptr = __file_ptr
	_file_pos = __file_pos
	index = _index
	size = _size
	pass


func get_file_pos() -> int:
	return _file_pos


func get_file_info() -> Dictionary:
	return {
		"eof_reached":		_file_ptr.eof_reached(),
		"length":			_file_ptr.get_len(),
		"pascal_string":	_file_ptr.get_pascal_string(),
		"path":				_file_ptr.get_path(),
		"absolute_path":	_file_ptr.get_path_absolute(),
		"position":			_file_ptr.get_position(),
		"is_open":			_file_ptr.is_open()
	}


func _to_string() -> String:
	return "{ name: %s, file_ptr: %s, file_pos: %d, index: %d, size: %d }"\
		% [name, _file_ptr, _file_pos, index, size]


func duplicate() -> LumpInfo:
	return get_script().new(name, _file_ptr, _file_pos, index, size)


func read_lump() -> Lump:
	var last_pos: int = _file_ptr.get_position()

	_file_ptr.seek(_file_pos)
	var data: PoolByteArray = _file_ptr.get_buffer(size)
	_file_ptr.seek(last_pos)

	return Lump.new(name, data)
